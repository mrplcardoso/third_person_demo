using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttack : MonoBehaviour, IDamage
{
	IAttack attacker;
	public float damage
	{ get { return attacker.damage; } }

	void Awake()
	{
		attacker =
			GetComponentsInParent<IAttack>()[0];
	}

	private void OnCollisionExit(Collision collision)
	{
		if (!attacker.attacking) { return; }

		Vector3 force = (collision.gameObject.transform.position -
		transform.position).normalized * damage;

		IAttackReaction reaction =
			collision.gameObject.GetComponent<IAttackReaction>();
		if (reaction != null)
		{ reaction.React(force); }
	}
}

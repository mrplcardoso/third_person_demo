using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCube : MonoBehaviour, ITimeScaler
{
	[SerializeField] Vector3[] path;
	Vector3 target { get { return path[index]; } }
	Vector3 direction { get { return target - transform.position; } }
	int index = 0;
	float speed = 5f;
	bool arrived { get { return Vector2.Distance(transform.position, target) < 0.2f; } }

	public float scalerTime { get; private set; }
	readonly float defaultTime = 1f;

	void Start()
	{
		scalerTime = defaultTime;
	}

	void Update()
	{
		transform.position += direction.normalized * speed * scalerTime * Time.deltaTime;
		if (arrived) { index = (index == 0) ? 1 : 0; }
	}

	public void ChangeTime(float time)
	{
		//resolve poss�veis problemas de precis�o de valores 'float'
		bool equal = (int)(scalerTime * 1000) == (int)(time * 1000);
		//Se a mudan�a de tempo foi aplicada e o novo tempo � igual ao anterior
		//Reseta a escala de tempo do objeto
		if (equal) { scalerTime = defaultTime; }
		//caso o novo tempo seja diferente, aplica-se o novo a escala de tempo do objeto
		else { scalerTime = time; }
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
	public Animator animator { get; private set; }
	
	Dictionary<string, AnimationClip> animationMap;
	Dictionary<string, Action> startEvents;
	Dictionary<string, Action> endEvents;

	private void Awake()
	{
		animator = GetComponent<Animator>();
	}

	private void Start()
	{
		startEvents = new Dictionary<string, Action>();
		endEvents = new Dictionary<string, Action>();

		animationMap = new Dictionary<string, AnimationClip>();
		AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
		for(int i = 0; i < clips.Length; ++i)
		{	animationMap.Add(clips[i].name, clips[i]); }

		SetAttack();
		SetLand();
	}

	void SetAttack()
	{
		AnimationEvent s = new AnimationEvent();
		s.time = 0; s.functionName = "OnStartAttack";
		AnimationEvent e = new AnimationEvent();
		e.time = animationMap["Female Sword Attack 1"].length;
		e.functionName = "OnEndAttack";

		animationMap["Female Sword Attack 1"].AddEvent(s);
		animationMap["Female Sword Attack 1"].AddEvent(e);
	}

	void SetLand()
	{
		AnimationEvent s = new AnimationEvent();
		s.time = 0;
		s.functionName = "OnStartLand";
		AnimationEvent e = new AnimationEvent();
		e.time = animationMap["Female Land"].length;
		e.functionName = "OnEndLand";

		animationMap["Female Land"].AddEvent(s);
		animationMap["Female Land"].AddEvent(e);
	}

	public void AddStartEvent(string animationName, Action animationEvent)
	{
		if(!animationMap.ContainsKey(animationName))
		{ Debug.LogError("No animation found"); }

		if(!startEvents.ContainsKey(animationName))
		{ startEvents.Add(animationName, null); }
		
		startEvents[animationName] -= animationEvent;
		startEvents[animationName] += animationEvent;
	}

	public void AddEndEvent(string animationName, Action animationEvent)
	{
		if (!animationMap.ContainsKey(animationName))
		{ Debug.LogError("No animation found"); }

		if (!endEvents.ContainsKey(animationName))
		{ endEvents.Add(animationName, null); }

		endEvents[animationName] -= animationEvent;
		endEvents[animationName] += animationEvent;
	}

	public void OnStartAttack(int i)
	{
		startEvents["Female Sword Attack 1"]();
	}

	public void OnEndAttack(int i)
	{
		endEvents["Female Sword Attack 1"]();
	}

	public void OnStartLand(int i)
	{
		startEvents["Female Land"]();
	}

	public void OnEndLand(int i)
	{
		endEvents["Female Land"]();
	}
}

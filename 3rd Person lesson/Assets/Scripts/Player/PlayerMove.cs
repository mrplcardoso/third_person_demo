using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
	PlayerUnit playerUnit;
	PlayerAnimator playerAnimator;
	CharacterController controller;

	//Dire��es de movimento em cada orienta��o
	//do personagem
	Vector3 xVelocity, yVelocity, zVelocity;

	//Vetor resultante de deslocamento
	Vector3 velocity
	{ get { return xVelocity + yVelocity + zVelocity; } }

	public float speed = 10f;
	float airSpeed { get { return speed * 0.5f; } }
	float jumpForce = 8f;
	const float gravity = 10f;

	//Se a vari�vel estiver verdadeira, o movimento
	//estar� travado,
	//se estiver falso o movimento estar� livre
	public bool lockMove
	{ get; private set; }

	private void Start()
	{
		controller = GetComponent<CharacterController>();
		playerUnit = GetComponent<PlayerUnit>();
		playerAnimator = GetComponentInChildren<PlayerAnimator>();
		lockMove = false;

		playerUnit.LateStartAction += AnimationLockEvent;
		playerUnit.FrameAction += MoveInput;
		playerUnit.FrameAction += JumpInput;
		playerUnit.FrameAction += Move;
	}

	void MoveInput()
	{
		//Quando movimento estiver travado
		//o m�todo deve ser encerrado imediatamente
		if (lockMove) return;

		//Seleciona bloco de movimento durante o pulo ou em solo
		if (controller.isGrounded)
		{
			//transform.right e transform.forward
			//guardam a dire��o do personagem/orienta��o
			//do personagem (para onde ele esta girado)
			xVelocity = transform.right * speed *
				Input.GetAxisRaw("Horizontal");
			zVelocity = transform.forward * speed *
				Input.GetAxisRaw("Vertical");
		}
		else
		{
			xVelocity = transform.right * airSpeed *
				Input.GetAxisRaw("Horizontal");
			zVelocity = transform.forward * airSpeed *
				Input.GetAxisRaw("Vertical");
		}

		Vector2 inputDirection = new Vector2(
			Input.GetAxisRaw("Horizontal"),	Input.GetAxisRaw("Vertical"));
		playerAnimator.animator.
			SetFloat("Run Blend", inputDirection.magnitude);
	}

	void JumpInput()
	{
		if (lockMove) return;

		yVelocity += Vector3.down * gravity * Time.deltaTime;

		if (controller.isGrounded)
		{ yVelocity = Vector3.down; }

		//Se o botao de pulo foi pressionado E o personagem 
		//se encontra no ch�o, aplicamos a for�a do pulo
		if (Input.GetKeyDown(KeyCode.Space) && controller.isGrounded)
		{ 
			yVelocity = Vector3.up * jumpForce; 
			playerAnimator.animator.SetTrigger("Jump"); 
		}
	}

	private void Move()
	{
		if (lockMove) return;

		controller.Move(velocity * Time.deltaTime);
	}

	void AnimationLockEvent()
	{
		playerAnimator.AddStartEvent("Female Sword Attack 1", LockMove);
		playerAnimator.AddEndEvent("Female Sword Attack 1", UnLockMove);
		playerAnimator.AddStartEvent("Female Land", LockMove);
		playerAnimator.AddEndEvent("Female Land", UnLockMove);
	}

	void LockMove()
	{
		lockMove = true;
	}

	void UnLockMove()
	{
		lockMove = false;
	}
}

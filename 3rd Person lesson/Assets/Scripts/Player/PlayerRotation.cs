using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
	PlayerUnit playerUnit;

	//Linkar camera pelo Inspector
	[SerializeField]
	Camera sceneCamera;

	//Linkado pelo Inspector
	//Posi��o da camera que acompanha 
	//o personagem
	[SerializeField]
	Transform cameraPosition;

	//Guarda a rota��o da vis�o do jogador
	float yRotation;
	[SerializeField]
	float sensitivity = 1f;//sensibilidade do mouse

	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;

		playerUnit = GetComponent<PlayerUnit>();

		playerUnit.FrameAction += RotationInput;
		playerUnit.FrameAction += Rotate;
		playerUnit.LateAction += FixCamera;
	}

	void RotationInput()
	{
		yRotation += 
			Input.GetAxisRaw("Mouse X") * sensitivity;
	}

	void Rotate()
	{
		sceneCamera.transform.eulerAngles = 
			new Vector3(0, yRotation, 0);
		transform.eulerAngles = new Vector3(0, yRotation, 0);
	}

	void FixCamera()
	{
		sceneCamera.transform.position = cameraPosition.position;
	}
}

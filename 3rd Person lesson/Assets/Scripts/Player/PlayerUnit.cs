using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnit : MonoBehaviour
{
	public Action LateStartAction;
	public Action FrameAction;
	public Action PhysicsAction;
	public Action LateAction;

	public void Start()
	{
		StartCoroutine(LateStart());
	}

	IEnumerator LateStart()
	{
		yield return new WaitForSeconds(0.1f);
		if (LateStartAction != null)
		{ LateStartAction(); }
	}

	public void Update()
	{
		if (FrameAction != null)
		{ FrameAction(); }
	}

	public void FixedUpdate()
	{
		if (PhysicsAction != null)
		{ PhysicsAction(); }
	}

	public void LateUpdate()
	{
		if (LateAction != null)
		{ LateAction(); }
	}
}
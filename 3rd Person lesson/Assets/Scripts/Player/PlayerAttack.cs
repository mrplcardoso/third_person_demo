using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour, IAttack
{
  PlayerAnimator playerAnimator;
  
  public Vector3 force { get; }
  [SerializeField]
  float playerDamage;
  public float damage
  { get { return playerDamage; } }

  public bool attacking { get; private set; }

  void Start()
	{
    playerAnimator = GetComponentInChildren<PlayerAnimator>();
    StartCoroutine(LateStart());
  }

  IEnumerator LateStart()
	{
    yield return new WaitForSeconds(0.01f);
    playerAnimator.AddStartEvent("Female Sword Attack 1", StartAttack);
    playerAnimator.AddEndEvent("Female Sword Attack 1", EndAttack);
  }

  void Update()
	{
    //Verifica se o jogador pressionou o bot�o esquerdo do mouse
    if(Input.GetMouseButtonDown(0))
    {
      playerAnimator.animator.SetTrigger("SwordAttack");
    }
	}

  void StartAttack()
	{
    attacking = true;
	}

  void EndAttack()
  {
    attacking = false;
  }
}
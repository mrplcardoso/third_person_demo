using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTimeControl : MonoBehaviour
{
	float controlRange = 7.5f;

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.E))
		{ SearchTimeScalers(); }
	}

	public void SearchTimeScalers()
	{
		Collider[] cols = Physics.OverlapSphere(transform.position, controlRange);
		if (cols.Length > 0)
		{
			for (int i = 0; i < cols.Length; ++i)
			{
				ITimeScaler scaler = cols[i].gameObject.GetComponent<ITimeScaler>();
				if (scaler != null)
				{
					scaler.ChangeTime(0.1f);
				}
			}
		}
	}
}

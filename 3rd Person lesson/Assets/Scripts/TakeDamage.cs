using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour, IAttackReaction
{
	public void React(Vector3 force)
	{
		force.y = 0;
		transform.position += force;
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimeScaler
{
	public float scalerTime
	{ get; }

	public void ChangeTime(float time);
}

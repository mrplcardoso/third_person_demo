using UnityEngine;

public interface IAttack
{
	public Vector3 force { get; }
	public float damage { get; }
	public bool attacking { get; }
}
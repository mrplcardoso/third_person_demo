using UnityEngine;

public interface IAttackReaction
{
	public void React(Vector3 force);
}